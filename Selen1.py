#!/usr/bin/env python
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import win32com.client
import unittest
import time
import os
from lxml.html import parse


def pth():
    global path_in
    path_in = raw_input(u'Enter a path of file with words:')
    if os.path.isfile(path_in) == False:
        print 'Wrong path or filename! Enter again'
        pth()
pth()
def pth_out():
    global path_out
    path_out = raw_input(u'Enter a path of output file:')
    if os.path.isfile(path_out) == True:
        print('Such file already exist! Please input other name.')
        pth_out()
    else:
        f_dir,f_name = os.path.split(path_out)
        ex = os.path.splitext(f_name)
        if os.path.isdir(f_dir) == False:
            print 'Wrong path! Enter again'
            pth_out()
        elif ex[1] != '.xls':
            print('File must be *.xls')
            pth_out()
pth_out()





# pathIn = '"' + pathIn + '"'
#path_out = 'r"' + path_out +'"'
#path_out = r'D:\work\tests\search\we.xls'
print(path_out)
list1 = []
list2 = []
list3 = []
# Получаем страничку
page = parse(path_in).getroot()
# Ищем все теги  с css классом text_field_result
hrefs = page.cssselect(".text_field_result")
hrefs2 = page.cssselect(".text_field_translate")
inc = zip(hrefs,hrefs2)

for row in inc:
    # Получаем атрибуты value
    list1.append(row[0].get("value"))
    list2.append(row[1].get("value"))


class Untitled(unittest.TestCase):



    def searching(self, item):
        try:
            self.driver.find_element_by_id("hunted_word").clear()
            self.driver.find_element_by_id("hunted_word").send_keys(item)
        except NoSuchElementException:
            self.searching(item)
      #  except NoSuchWindowException:
       #     self.searching(item)
        else:
            try:
                self.driver.find_element_by_id("hunted_word_submit").click()
                time.sleep(1)
                element = self.driver.find_element_by_css_selector('#uk_tr_sound span')
            except NoSuchElementException:
                words = item.split(' ')
                if len(words) > 1:
                    try:
                        temp = ' '
                        for word in words:
                            temp+=self.searching(word)[1:-1]+' '
                        Result2 = '|'+temp[:-1]+'|'
                        return Result2
                    except Exception:
                        Result = 'No such element'
                        return Result
                else:
                    Result = 'No such element'
                    return Result
        #    except NoSuchWindowException:
         #       self.searching(item)
            else:
                try:
                    Result = element.text
                    if Result == None:
                        self.searching(item)
                    else:
                        return Result
                except Exception:
                    self.searching(item)



    def setUp(self):
        self.driver = webdriver.Firefox()
        #задаем неявные ожидания
        self.driver.implicitly_wait(7)
        self.base_url = "http://wooordhunt.ru/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_untitled(self):
        driver = self.driver
        driver.get(self.base_url + "/word/hair")

        #перебираем все наши слова
        for my_tuple in list1:
            my_tuple = my_tuple.strip()
            trans = self.searching(my_tuple)
            list3.append(trans)

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)
        #сгруппируем все в список кортежей
        new_inc = zip(list1,list2,list3)


        # начинаем работать с Exel ф-лами
        Excel = win32com.client.Dispatch("Excel.Application")
        #wb = Excel.Workbooks.Open(r'D:\Work\tests\test1\x1.xls')
        #создаем новую книгу
        wb = Excel.Workbooks.Add()
        sheet = wb.ActiveSheet
        i = 1
        for item in new_inc:
            sheet.Cells(i,2).value = item[0]
            sheet.Cells(i,3).value = item[2]
            sheet.Cells(i,4).value = item[1]
            i+=1
        else: i = 1


        #сохраняем рабочую книгу
        wb.SaveAs(path_out)

        #закрываем ее
        wb.Close()

        #закрываем COM объект
        Excel.Quit()


if __name__ == "__main__":
    unittest.main()


